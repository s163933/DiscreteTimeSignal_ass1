%% Fourier spectrums

fs = 8000; % Sample fq chosen at leisure (> 2*500hz) 
%            Weird if fs = 8000 vs 8003
A = 1;
f = 500;
T = 0.5;
t = 0:1/fs:T-1/fs; % Time vector

x1 = A*sin(t*f*2*pi); % Generate sinusoid

% Find the spectrum
[X1,frec] = make_spectrum(x1,fs);
N = length(X1);
X1 = X1(N/2:N,1); 
frec = frec(N/2:N,1);
% Find the absolute value in dB
X1_db = 20*log10(abs(X1));

% Plot the first signal
figure(1)
plot(frec,X1_db)
hold on 

% Adding a second signal
f = 499; % [hz]
x2 = A*sin(t*f*2*pi); % Generate sinusoid
% Find the spectrum, and find the absolute value in dB
[X2,frec] = make_spectrum(x2,fs);
X2_db = 20*log10(abs(X2));

% Plot the second signal
plot(frec,X2_db);
hold off
% Adjust the graph to be nice to look at.
legend(['500 Hz';'499 Hz'])
xlim([0 700]);
ylim([-60 0]);
xlabel('Frequency [Hz]');
ylabel('Amplitude [dB]');

% Save for latex export
% saveas(gcf,'fft_sinusoids','epsc')

% Comments on figure: yeah, they are a bit weird, is fft doing somethign weird
% compared to a non-fast fourier transform?

%% 1.2 Fourier transform of a synthetic signal

% Define constants
fs = 1000; % Sampling frequency
T = 4; % Time of signal
t = 0:1/fs:T-1/fs; % Time vector
f0 = 25; % Fundamental frequency
s = t*0; % Initialize signal vector for speed
% Generate the described signal
for k = 0:4
    s = s + cos(2*pi*2^k*f0*t+k*pi/3); % We add all the signals together 
end

% Plot the signal in time
figure(2)
plot(t,s);
xlim([0.8 0.9]); % Limit the output view
title('Signal s(n), showing parts 0.8s to 0.9s');
xlabel('Time [s]');
ylabel('Amplitude');
% Save the signal
% saveas(gcf,'4sigs-parts','epsc')

% Generate a complex spectrum of the signal
% [S, frec] = make_spectrum(s, fs);
[Y,freq]=make_spectrum(s,fs);
N=length(Y);
Y=Y(N/2:N,1);
freq=freq(N/2:N,1);
% Find the magnitude
magnitude=abs(Y);
% The phase part has lots of noise, so we only find the phase of the parts
% with a magnitude above 0.1, since the signals has a magnitude of 1.
phase = zeros(length(Y));
phase(abs(Y) > 0.1) = angle(Y(abs(Y) > 0.1));
% A less optimized version of the code above:
% for i=1:N/2
%     if magnitude(i)>0.1
%         phase(i)=angle(Y(i));
%     end
% end

% Show the magnitude, phase, real- and imaginary parts in one figure.
figure(3);
P1=subplot(2,2,1); plot(freq,magnitude);xlabel('Frequencies'); ylabel('Magnitude');
P3=subplot(2,2,3); plot(freq,phase); xlabel('Frequencies'); ylabel('Phase');
P2=subplot(2,2,2); plot(freq,real(Y)); xlabel('Frequencies'); ylabel('Real part');
P4=subplot(2,2,4); plot(freq,imag(Y)); xlabel('Frequencies'); ylabel('Imaginary part');
set(P3,'YLim', [-pi pi]);

% Save figure for export
% saveas(gcf, '5sig-allspec', 'epsc');

%% Plot the magnitudes of the frequencies
figure(5)
semilogx(freq, abs(Y));
title('Magnitude of S(k)');
ylabel('Magnitude [dB]')
xlabel('Frequencies [Hz]')
xlim([0 fs/2]);
% Save figure for export
%saveas(gcf,'5sigs-logmag','epsc')

%% Compressing
% Save the audio
s_scaled = s/4; % Scaled to have an amplitude of 1 to avoid clipping
filename = 'HandsOn3Audio.wav'; % Specify name
audiowrite(filename, s_scaled, fs, 'BitsPerSample', 16); % 16 bit debth

% Load the audio
[s_loaded, fs_loaded] = audioread(filename);

% Show the original and the saved+loaded signal
figure(6)
plot(t,s)
hold on
plot(t,s_loaded)
legend('Original signal', 'Loaded signal');
title('Comparison of s(n) and a compressed s(n)');
hold off;
xlim([0.85 0.925])
saveas(gcf,'5sigs-compress','epsc')