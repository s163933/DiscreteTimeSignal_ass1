fs = 1000; %[Hz]
Ttot = 0.1; % [s]
square = generate_square(fs, 0.010);
square = zero_pad(square, fs, Ttot);
ramp = generate_ramp(fs, 0.01);
ramp = zero_pad(ramp, fs, Ttot);
t = 0:1/fs:Ttot-1/fs;

% % Check signals
% figure(1)
% plot(t,ramp);
% hold on
% plot(t, square);
% hold off
% legend('Ramp', 'Square');

op1 = conv(square, square);
op2 = conv(ramp, square);
op3 = conv(ramp, ramp);

% figure(2)
% plot(op1);
% hold on
% plot(op2);
% plot(op3);
% hold off
%%
%Generate impulse response
fs = 10000; % 10 kHz
t = 0:1/fs:0.1-1/fs;
delta = t;
delta(:) = 0;
delta(1) = 1;

movAvg = dsp.MovingAverage('Method','Sliding window', 'WindowLength', 21);
x1 = movAvg(delta);
x2 = zeros(1, length(delta));
order = 21;

for k=1:length(delta)
    for n=0:order-1
        index = k-n;
        if index < 1
            index = 1;
        end
        % Add the last order of the input function (delta)
        x2(k) = x2(k) + delta(index); 
    end
    x2(k) = x2(k)/order;
end




