function filtered_signal = sumfilter(signal,sum_iterations)
% Filters "signal" with a moving average of order "sum_iterations"

filtered_signal=zeros(length(signal),1);
% For each signal value, average the last N values.
for i=1:length(filtered_signal)
    sum = signal(i);
    for j=1:sum_iterations-1
        if (i-j)<1
            sum = sum; % Included for readability, does nothing.
        else
            sum = sum+signal(i-j);
        end
    end
    filtered_signal(i) = sum/sum_iterations;
end
