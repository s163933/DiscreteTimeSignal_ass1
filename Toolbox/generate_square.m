function signal = generate_square(fs, T0)
%GENERATE_SQUARE Noarmalised square with duration T0
%   Detailed explanation goes here
t = 0:1/fs:T0-1/fs;
signal = ones(1,length(t));
signal = signal * (1/T0);
end

