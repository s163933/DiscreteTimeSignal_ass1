% Define constants for signal generation
t1=0.1;
fs=10000;
% Generate impulse/dirac
impulse=zeros(t1*fs,1);
impulse(1)=1;
% Find the impulse response of an order 21 moving average
impulse_response21=sumfilter(impulse,21);
% Create time vectors
time1=linspace(0,0.1-1/fs,0.1*fs);
time2=linspace(0,1-1/fs,fs);
% Generate sinusoidal signals
sin1=genSinusoid(fs,1,500,1);
sin2=genSinusoid(fs,1,2200,1);
sin3=genSinusoid(fs,1,4050,1);

% Plot the signals
figure(1)
plot(time2,sin1,'r')
hold on
xlim([0,2/500])
plot(time2,sin2,'b')
plot(time2,sin3,'g')
plot(time1,impulse_response21,'c')
hold off
% Labels
xlabel('Time [s]');
ylabel('Amplitude');
legend('Sin, 500Hz', 'Sin, 2200Hz', 'Sin, 4050Hz', 'Impulse response');
% Save
% saveas(gcf,'3unfiltered_signals','epsc');

%% Convolutions

% Convolute all the signals with the moving average
sin1_conv=conv(sin1,impulse_response21,'same');
figure(2)
subplot(3,1,1),plot(time2,sin1_conv,time2,sin1),xlabel('time'),ylabel('500hz');
xlim([0,1/500])
legend('Filtered', 'Unfiltered');

sin2_conv=conv(sin2,impulse_response21,'same');
subplot(3,1,2),plot(time2,sin2_conv,time2,sin2),xlabel('time'),ylabel('2200hz');
xlim([0,1/500])
legend('Filtered', 'Unfiltered');

sin3_conv=conv(sin3,impulse_response21,'same');
subplot(3,1,3),plot(time2,sin3_conv,time2,sin3),xlabel('time'),ylabel('4050hz');
xlim([0,1/500])
legend('Filtered', 'Unfiltered');
% Save
% saveas(gcf,'filtered_signals','epsc');

%% Generate the next thing, but higher sampling. (unused)
sin4=genSinusoid(fs,1,10000/21,1);
impulse_responsesin4=sumfilter(sin4,21);

sin4_conv=conv(sin4,impulse_response21,'same');
figure(3)
plot(time2,sin4_conv,time2,sin4)
xlim([0,1/500])

%% Eigth part
% Define new signal
fs=50;
f=10;
T=1;
A=1;
time3=linspace(0,1-1/fs,fs);
% gen signal
sin5=genSinusoid(fs,T,f,A);
% find impulse respons of order 5 running average
impulse_response5=sumfilter(impulse,5);
% Convolute signal and filter
sin5_conv=conv(sin5,impulse_response5,'same');
% Show
figure(4)
plot(time3,sin5_conv,time3,sin5)
xlim([0,1/f])
xlabel('Time [s]');
ylabel('Ampliude');
legend('Filtered', 'Unfiltered');
% saveas(gcf,'completely_filtered_signals','epsc');
